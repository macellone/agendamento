Frameworks e linguagens:

- Java 8;
- Maven (Para gerenciar dependências e build);
- Spring MVC com Java Config (Para disponibilizar Endpoints);
- jackson-databind (para marshall e unmarshall de Json pelo Spring)
- JUnit e Hamcrest (Para criar testes de unidade mais acertivos)

Design Patterns Utilizados:
Uma combinação de Strategy, Template Method e Chain of Responsibility para determinar com mais clareza o tipo de taxa aplicável ou nao, maior aproveitamento de código e para que cada tipo de calculo de taxa fique o mais isolado possivel.

Persistência:
Foi utilizado um wrap de um Map para atender aos seguintes requisitos/motivos:
-Nao utilizar banco de dados relacional;
-Utilizar persistencia in-memory;
-Não ter que configurar nada na máquina de quem for clonar e executar este projeto;
-Simplicidade;

Como testar esta aplicação:
- Clonar o repositório;
- Executar o comando na pasta raiz do repositorio: mvn clean install jetty:run

Exemplo de envio de agendamento:
curl -i -H "Content-type: application/json" -H "Accept: application/json" -X POST -d '{"contaOrigem":"123456", "contaDestino" : "987654", "valorTransferencia" : "10000.00", "dataTransferencia" : "07/11/2017", "tipoOperacao" : "D"}' http://localhost:8080/

Exemplo de como listar os agendamentos:
curl -H "Accept: application/json" -X GET  http://localhost:8080/
