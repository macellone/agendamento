package br.com.macellone.agendamento.model;

import br.com.macellone.agendamento.strategy.OperacaoA;
import br.com.macellone.agendamento.strategy.OperacaoB;
import br.com.macellone.agendamento.strategy.OperacaoC;
import br.com.macellone.agendamento.strategy.OperacaoD;
import br.com.macellone.agendamento.strategy.TipoOperacaoStrategy;

public enum TipoOperacao {

	A(new OperacaoA()), 
	B(new OperacaoB()), 
	C(new OperacaoC()), 
	D(new OperacaoD());
	
	private TipoOperacaoStrategy tipoOperacaoStrategy;
	
	private TipoOperacao(TipoOperacaoStrategy strategy) {
		tipoOperacaoStrategy = strategy;
	}
	
	public TipoOperacaoStrategy getOperacaoStrategy() {
		return tipoOperacaoStrategy;
	}
	
}