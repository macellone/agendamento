package br.com.macellone.agendamento.model;

import java.math.BigDecimal;
import java.time.LocalDate;

import br.com.macellone.agendamento.service.CalculadoraTaxa;

public class Operacao {

	private Long id;
	private final String contaOrigem;
	private final String contaDestino;
	private final BigDecimal valorTransferencia;
	private final LocalDate dataAgendamento;
	private final LocalDate dataTransferencia;
	private final TipoOperacao tipoOperacao;
	private BigDecimal taxa;

	Operacao(String contaOrigem, 
			String contaDestino, 
			BigDecimal valorTransferencia, 
			LocalDate dataAgendamento,
			LocalDate dataTransferencia, 
			TipoOperacao tipoOperacao) {

		this.contaOrigem = contaOrigem;
		this.contaDestino = contaDestino;
		this.valorTransferencia = valorTransferencia;
		this.dataAgendamento = dataAgendamento;
		this.dataTransferencia = dataTransferencia;
		this.tipoOperacao = tipoOperacao;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getContaOrigem() {
		return contaOrigem;
	}

	public String getContaDestino() {
		return contaDestino;
	}

	public BigDecimal getValorTransferencia() {
		return valorTransferencia;
	}

	public LocalDate getDataTransferencia() {
		return dataTransferencia;
	}

	public LocalDate getDataAgendamento() {
		return dataAgendamento;
	}
	
	public TipoOperacao getTipoOperacao() {
		return tipoOperacao;
	}
	
	public BigDecimal getTaxa() {
		return taxa;
	}

	public Operacao calcular() {
		CalculadoraTaxa calculadora = new CalculadoraTaxa(this);
		taxa = calculadora.calcular();
		return this;
	}

}