package br.com.macellone.agendamento.model;

import java.math.BigDecimal;
import java.time.LocalDate;

public class OperacaoBuilder {

	private String contaOrigem;
	private String contaDestino;
	private BigDecimal valorTransferencia;
	private LocalDate dataAgendamento = LocalDate.now();
	private LocalDate dataTransferencia;
	private TipoOperacao tipoOperacao;
	private boolean hasBeenBuilt = false;
	
	private static final String CONTA_REGEXP = "^\\d{6}$";
	

	public OperacaoBuilder withContaOrigem(String contaOrigem) {
		validateConta(contaOrigem);
		this.contaOrigem = contaOrigem;
		return this;
	}

	public OperacaoBuilder withContaDestino(String contaDestino) {
		validateConta(contaDestino);
		this.contaDestino = contaDestino;
		return this;
	}

	public OperacaoBuilder withValorTransferencia(BigDecimal valorTransferencia) {
		validateValorTransferencia(valorTransferencia);
		this.valorTransferencia = valorTransferencia;
		return this;
	}

	public OperacaoBuilder withDataTransferencia(LocalDate dataTransferencia) {
		validateDataTransferencia(dataTransferencia);
		this.dataTransferencia = dataTransferencia;
		return this;
	}

	public OperacaoBuilder withTipoOperacao(TipoOperacao tipoOperacao) {
		this.tipoOperacao = tipoOperacao;
		return this;
	}
	
	public Operacao build() {
		validateState();
		Operacao operacao = new Operacao(contaOrigem, contaDestino, valorTransferencia, dataAgendamento, dataTransferencia, tipoOperacao);
		hasBeenBuilt = true;
		return operacao;
	}
	
	private void validateConta(String conta) {
		if (conta == null || !conta.matches(CONTA_REGEXP)) {
			throw new IllegalArgumentException("Conta deve conter 6 dígitos numéricos");
		}
	}
	
	private void validateDataTransferencia(LocalDate dataTransferencia) {
		if (dataTransferencia == null) {
			throw new IllegalArgumentException("Data de transferencia não pode ser nula");
		}
		if (dataTransferencia.isBefore(dataAgendamento)) {
			throw new IllegalArgumentException("Data de transferencia não pode ser anterior a hoje");
		}
	}
	
	private void validateValorTransferencia(BigDecimal valorTransferencia2) {
		if (valorTransferencia2 == null || valorTransferencia2.compareTo(BigDecimal.ZERO) <= 0) {
			throw new IllegalArgumentException("Valor da transferência deve ser maior do que zero");
		}
	}
	
	private void validateState() {
		if (hasBeenBuilt) {
			throw new IllegalStateException("Operacao ja foi criada.");
		}
		if (contaOrigem == null || contaDestino == null || valorTransferencia == null || dataTransferencia == null || tipoOperacao == null) {
			throw new IllegalStateException("Nenhum valor pode ser nulo");
		}
	}

}