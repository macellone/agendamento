package br.com.macellone.agendamento.service;

import java.math.BigDecimal;

import br.com.macellone.agendamento.model.Operacao;

public class CalculadoraTaxa {

	private Operacao operacao;

	public CalculadoraTaxa(Operacao operacao) {
		this.operacao = operacao;
	}

	public BigDecimal calcular() {
		return operacao.getTipoOperacao()
				.getOperacaoStrategy()
				.calcular(operacao);
	}

}