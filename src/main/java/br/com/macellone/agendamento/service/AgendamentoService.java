package br.com.macellone.agendamento.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.macellone.agendamento.controller.OperacaoDTO;
import br.com.macellone.agendamento.model.Operacao;
import br.com.macellone.agendamento.repository.AgendamentoRepository;
import br.com.macellone.agendamento.utils.OperacaoDTOConverter;

@Service
public class AgendamentoService {

	@Autowired
	private AgendamentoRepository agendamentoRepository;

	public Long processarTransferencia(OperacaoDTO operacaoDTO) {
		Operacao operacao = OperacaoDTOConverter.toOperacaoFrom(operacaoDTO).calcular();
		return agendamentoRepository.save(operacao);
	}

	public List<OperacaoDTO> findAll() {
		List<Operacao> operacoes = agendamentoRepository.findAll();
		List<OperacaoDTO> dtos = new ArrayList<>();

		for (Operacao o : operacoes) {
			dtos.add(OperacaoDTOConverter.toOperacaoDTOFrom(o));
		}

		return dtos;
	}

}
