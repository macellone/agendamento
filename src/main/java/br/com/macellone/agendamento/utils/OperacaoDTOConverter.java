package br.com.macellone.agendamento.utils;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import br.com.macellone.agendamento.controller.OperacaoDTO;
import br.com.macellone.agendamento.model.Operacao;
import br.com.macellone.agendamento.model.OperacaoBuilder;

public class OperacaoDTOConverter {

	public static Operacao toOperacaoFrom(OperacaoDTO operacaoDTO) {
		LocalDate dataTransferencia = LocalDate.parse(operacaoDTO.getDataTransferencia(), DateTimeFormatter.ofPattern("dd/MM/yyyy"));
		
		return new OperacaoBuilder()
				.withContaOrigem(operacaoDTO.getContaOrigem())
				.withContaDestino(operacaoDTO.getContaDestino())
				.withDataTransferencia(dataTransferencia)
				.withValorTransferencia(operacaoDTO.getValorTransferencia())
				.withTipoOperacao(operacaoDTO.getTipoOperacao())
				.build();
	}
	
	public static OperacaoDTO toOperacaoDTOFrom(Operacao o) {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
		OperacaoDTO dto = new OperacaoDTO();
		dto.setId(o.getId());
		dto.setContaDestino(o.getContaDestino());
		dto.setContaOrigem(o.getContaOrigem());
		dto.setDataTransferencia(formatter.format(o.getDataTransferencia()));
		dto.setTipoOperacao(o.getTipoOperacao());
		dto.setValorTransferencia(o.getValorTransferencia());
		dto.setTaxa(o.getTaxa());
		dto.setDataAgendamento(formatter.format(o.getDataAgendamento()));
		return dto;
	}
	
}
