package br.com.macellone.agendamento.repository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import br.com.macellone.agendamento.model.Operacao;

@Repository
public class AgendamentoRepository {

	private static final Map<Long, Operacao> map = new HashMap<>();
	private static Long sequence = 1L;

	private static Long getNextVal() {
		return sequence++;
	}
	
	public Long save(Operacao operacao) {
		Long id = getNextVal();
		operacao.setId(id);
		map.put(id, operacao);
		return id;
	}

	public Operacao find(Long id) {
		return map.get(id);
	}

	public List<Operacao> findAll() {
		return new ArrayList<>(map.values());
	}

	public Operacao delete(Long id) {
		return map.remove(id);
	}

}