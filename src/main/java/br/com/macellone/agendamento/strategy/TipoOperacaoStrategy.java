package br.com.macellone.agendamento.strategy;

import java.math.BigDecimal;

import br.com.macellone.agendamento.model.Operacao;

public interface TipoOperacaoStrategy {

	BigDecimal calcular(Operacao operacao);
}