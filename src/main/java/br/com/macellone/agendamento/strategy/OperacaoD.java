package br.com.macellone.agendamento.strategy;

import java.math.BigDecimal;

import br.com.macellone.agendamento.chainofresponsibility.OperacaoDAcimaDe2000;
import br.com.macellone.agendamento.chainofresponsibility.OperacaoDAte1000;
import br.com.macellone.agendamento.chainofresponsibility.OperacaoDAte2000;
import br.com.macellone.agendamento.model.Operacao;

public class OperacaoD implements TipoOperacaoStrategy {

	@Override
	public BigDecimal calcular(Operacao operacao) {
		return new OperacaoDAte1000(new OperacaoDAte2000(new OperacaoDAcimaDe2000())).getTaxa(operacao);
	}

}