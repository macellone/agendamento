package br.com.macellone.agendamento.strategy;

import java.math.BigDecimal;
import java.time.LocalDate;

import br.com.macellone.agendamento.model.Operacao;

public class OperacaoA implements TipoOperacaoStrategy {

	private static final BigDecimal VALOR_FIXO = BigDecimal.valueOf(3);
	private static final BigDecimal TAXA = BigDecimal.valueOf(0.03);

	@Override
	public BigDecimal calcular(Operacao operacao) {
		validar(operacao);
		BigDecimal result = operacao.getValorTransferencia().multiply(TAXA).add(VALOR_FIXO);
		return result;

	}

	private void validar(Operacao operacao) {
		LocalDate dataAgendamento = operacao.getDataAgendamento();
		LocalDate dataTransferencia = operacao.getDataTransferencia();
		if (!dataAgendamento.isEqual(dataTransferencia)) {
			throw new UnsupportedOperationException("Nenhuma taxa pode ser aplicada para esta operação");
		}
	}

}