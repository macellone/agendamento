package br.com.macellone.agendamento.strategy;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

import br.com.macellone.agendamento.model.Operacao;

public class OperacaoB implements TipoOperacaoStrategy {

	private static final BigDecimal VALOR_FIXO = BigDecimal.valueOf(12);

	@Override
	public BigDecimal calcular(Operacao operacao) {
		validar(operacao);
		return VALOR_FIXO;
	}

	private void validar(Operacao operacao) {
		LocalDate dataLimiteParaTransferencia = operacao.getDataAgendamento().plus(10L, ChronoUnit.DAYS);
		LocalDate dataTransferencia = operacao.getDataTransferencia();

		if (dataTransferencia.isAfter(dataLimiteParaTransferencia)) {
			throw new UnsupportedOperationException("Nenhuma taxa pode ser aplicada para esta operação");
		}
	}

}