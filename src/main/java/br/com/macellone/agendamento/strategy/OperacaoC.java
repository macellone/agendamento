package br.com.macellone.agendamento.strategy;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

import br.com.macellone.agendamento.chainofresponsibility.OperacaoCAcimaDe40Dias;
import br.com.macellone.agendamento.chainofresponsibility.OperacaoCAcimaDeDezDias;
import br.com.macellone.agendamento.chainofresponsibility.OperacaoCAcimaDeTrintaDias;
import br.com.macellone.agendamento.chainofresponsibility.OperacaoCAcimaDeVinteDias;
import br.com.macellone.agendamento.model.Operacao;

public class OperacaoC implements TipoOperacaoStrategy {

	@Override
	public BigDecimal calcular(Operacao operacao) {
		validar(operacao);
		return new OperacaoCAcimaDeDezDias(new OperacaoCAcimaDeVinteDias(new OperacaoCAcimaDeTrintaDias(new OperacaoCAcimaDe40Dias()))).getTaxa(operacao);
	}
	
	private void validar(Operacao operacao) {
		LocalDate dataMinimaParaTransferencia = operacao.getDataAgendamento().plus(11L, ChronoUnit.DAYS);
		LocalDate dataTransferencia = operacao.getDataTransferencia();
		
		if (dataTransferencia.isBefore(dataMinimaParaTransferencia)) {
			throw new UnsupportedOperationException("Nenhuma taxa pode ser aplicada para esta operação");
		}
	}

}