package br.com.macellone.agendamento.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("br.com.macellone.agendamento")
public class ApplicationConfiguration {

}
