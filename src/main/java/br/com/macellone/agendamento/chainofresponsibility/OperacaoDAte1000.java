package br.com.macellone.agendamento.chainofresponsibility;

import java.math.BigDecimal;

import br.com.macellone.agendamento.model.Operacao;
import br.com.macellone.agendamento.strategy.OperacaoA;

public class OperacaoDAte1000 extends OperacaoDTemplate {

	private static final BigDecimal MIL = BigDecimal.valueOf(1000);

	public OperacaoDAte1000() {
	}

	public OperacaoDAte1000(OperacaoDTemplate nextTemplate) {
		super(nextTemplate);
	}

	@Override
	protected boolean isAplicavel(Operacao operacao) {
		return operacao.getValorTransferencia().compareTo(MIL) <= 0;
	}

	@Override
	protected BigDecimal calcularTaxa(Operacao operacao) {
		return new OperacaoA().calcular(operacao);
	}

}