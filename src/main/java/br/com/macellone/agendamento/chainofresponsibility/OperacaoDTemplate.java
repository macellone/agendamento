package br.com.macellone.agendamento.chainofresponsibility;

import java.math.BigDecimal;

import br.com.macellone.agendamento.model.Operacao;

public abstract class OperacaoDTemplate {

	protected OperacaoDTemplate nextTemplate;

	public OperacaoDTemplate() {
	}

	public OperacaoDTemplate(OperacaoDTemplate nextTemplate) {
		this.nextTemplate = nextTemplate;
	}

	public final BigDecimal getTaxa(Operacao operacao) {
		if (isAplicavel(operacao)) {
			return calcularTaxa(operacao).setScale(2, BigDecimal.ROUND_HALF_UP);
		} else if (nextTemplate != null) {
			return nextTemplate.getTaxa(operacao);
		} else {
			throw new UnsupportedOperationException("Nenhuma taxa pode ser aplicada para esta operação");
		}
	}

	protected abstract BigDecimal calcularTaxa(Operacao operacao);

	protected abstract boolean isAplicavel(Operacao operacao);

}