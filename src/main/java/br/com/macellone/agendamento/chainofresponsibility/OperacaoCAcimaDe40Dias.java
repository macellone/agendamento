package br.com.macellone.agendamento.chainofresponsibility;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

import br.com.macellone.agendamento.model.Operacao;

public class OperacaoCAcimaDe40Dias extends OperacaoCTemplate {

	private static final Long A_PARTIR_QUANTOS_DIAS = 40L;
	private static final BigDecimal TAXA = BigDecimal.valueOf(-0.017);

	public OperacaoCAcimaDe40Dias() {
	}

	public OperacaoCAcimaDe40Dias(OperacaoCTemplate proximoTemplate) {
		super(proximoTemplate);
	}

	@Override
	protected boolean isAplicavel(Operacao operacao) {
		LocalDate dataTransferencia = operacao.getDataTransferencia();

		LocalDate dataAgendamento = operacao.getDataAgendamento();
		LocalDate dateFrom = dataAgendamento.plus(A_PARTIR_QUANTOS_DIAS, ChronoUnit.DAYS);

		if (dataTransferencia.isAfter(dateFrom)) {
			return true;
		}

		return false;
	}

	@Override
	protected BigDecimal getPorcentagemTaxa() {
		return TAXA;
	}
}