package br.com.macellone.agendamento.chainofresponsibility;

import java.math.BigDecimal;

import br.com.macellone.agendamento.model.Operacao;
import br.com.macellone.agendamento.strategy.OperacaoB;

public class OperacaoDAte2000 extends OperacaoDTemplate {

	private static final BigDecimal MIL = BigDecimal.valueOf(1000);
	private static final BigDecimal DOIS_MIL = BigDecimal.valueOf(2000);

	public OperacaoDAte2000() {
	}

	public OperacaoDAte2000(OperacaoDTemplate nextTemplate) {
		super(nextTemplate);
	}

	@Override
	protected BigDecimal calcularTaxa(Operacao operacao) {
		return new OperacaoB().calcular(operacao);
	}

	@Override
	protected boolean isAplicavel(Operacao operacao) {
		BigDecimal valorTransferencia = operacao.getValorTransferencia();
		return valorTransferencia.compareTo(MIL) > 0 && valorTransferencia.compareTo(DOIS_MIL) <= 0;
	}
}