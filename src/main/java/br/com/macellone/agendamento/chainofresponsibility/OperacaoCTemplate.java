package br.com.macellone.agendamento.chainofresponsibility;

import java.math.BigDecimal;

import br.com.macellone.agendamento.model.Operacao;

public abstract class OperacaoCTemplate {

	protected OperacaoCTemplate proximoTemplate;

	public OperacaoCTemplate() {
	}

	public OperacaoCTemplate(OperacaoCTemplate proximoTemplate) {
		this.proximoTemplate = proximoTemplate;
	}

	public final BigDecimal getTaxa(Operacao operacao) {
		if (isAplicavel(operacao)) {
			return calcularTaxa(operacao).setScale(2, BigDecimal.ROUND_HALF_UP);
		} else if (proximoTemplate != null) {
			return proximoTemplate.getTaxa(operacao);
		} else {
			throw new UnsupportedOperationException("Nenhuma taxa pode ser aplicada para esta operação");
		}
	}

	private BigDecimal calcularTaxa(Operacao operacao) {
		return operacao.getValorTransferencia().multiply(getPorcentagemTaxa());
	}

	protected abstract boolean isAplicavel(Operacao operacao);

	protected abstract BigDecimal getPorcentagemTaxa();

}