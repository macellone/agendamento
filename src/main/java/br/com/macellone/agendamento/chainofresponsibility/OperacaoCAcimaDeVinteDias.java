package br.com.macellone.agendamento.chainofresponsibility;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

import br.com.macellone.agendamento.model.Operacao;

public class OperacaoCAcimaDeVinteDias  extends OperacaoCTemplate {
	
	private static final Long A_PARTIR_QUANTOS_DIAS = 20L;
	private static final Long FAIXA_DIAS_EXCLUDENTE = 11L;

	private static final BigDecimal TAXA = BigDecimal.valueOf(0.069);
	
	public OperacaoCAcimaDeVinteDias() {
	}
	
	public OperacaoCAcimaDeVinteDias(OperacaoCTemplate proximoTemplate) {
		super(proximoTemplate);
	}

	@Override
	protected boolean isAplicavel(Operacao operacao) {
		LocalDate dataTransferencia = operacao.getDataTransferencia();

		LocalDate dataAgendamento = operacao.getDataAgendamento();
		LocalDate dateFrom = dataAgendamento.plus(A_PARTIR_QUANTOS_DIAS, ChronoUnit.DAYS);
		LocalDate dateTo = dataAgendamento.plus(A_PARTIR_QUANTOS_DIAS + FAIXA_DIAS_EXCLUDENTE, ChronoUnit.DAYS);

		if (dataTransferencia.isAfter(dateFrom) && dataTransferencia.isBefore(dateTo)) {
			return true;
		}

		return false;
	}

	@Override
	protected BigDecimal getPorcentagemTaxa() {
		return TAXA;
	}

}
