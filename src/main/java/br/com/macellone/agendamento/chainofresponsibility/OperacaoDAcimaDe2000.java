package br.com.macellone.agendamento.chainofresponsibility;

import java.math.BigDecimal;

import br.com.macellone.agendamento.model.Operacao;
import br.com.macellone.agendamento.strategy.OperacaoC;

public class OperacaoDAcimaDe2000 extends OperacaoDTemplate {

	private static final BigDecimal DOIS_MIL = BigDecimal.valueOf(2000);

	public OperacaoDAcimaDe2000() {
	}

	public OperacaoDAcimaDe2000(OperacaoDTemplate nextTemplate) {
		super(nextTemplate);
	}
	
	@Override
	protected boolean isAplicavel(Operacao operacao) {
		return operacao.getValorTransferencia().compareTo(DOIS_MIL) > 0;
	}

	@Override
	protected BigDecimal calcularTaxa(Operacao operacao) {
		return new OperacaoC().calcular(operacao);
	}

}