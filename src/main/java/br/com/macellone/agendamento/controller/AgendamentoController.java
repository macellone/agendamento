package br.com.macellone.agendamento.controller;

import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;
import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8_VALUE;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.macellone.agendamento.service.AgendamentoService;

@RestController
@RequestMapping(path = "/", produces = APPLICATION_JSON_UTF8_VALUE)
public class AgendamentoController {

	@Autowired
	private AgendamentoService agendamentoService;

	@RequestMapping(path = "/", method = POST, consumes = APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<String> agendarTransferencia(@RequestBody OperacaoDTO operacao) {
		Long newOperacaoId = null;
		try {
			newOperacaoId = agendamentoService.processarTransferencia(operacao);
		} catch (UnsupportedOperationException e) {
			return new ResponseEntity<String>("Transferência não aceita", BAD_REQUEST);
		} catch (Exception e) {
			return new ResponseEntity<String>("Erro ao agendar transferência", INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<String>("{\"id\" : \"" + newOperacaoId + "\"}", CREATED);

	}

	@RequestMapping(path = "/", method = GET)
	public List<OperacaoDTO> listarTransferencias(HttpServletRequest request, HttpServletResponse response) {
		return agendamentoService.findAll();
	}

}