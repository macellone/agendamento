package br.com.macellone.agendamento.model;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

import org.junit.Before;
import org.junit.Test;

public class OperacaoBuilderTest {

	private OperacaoBuilder builder;

	@Before
	public void withUp() {
		builder = new OperacaoBuilder();
	}

	@Test
	public void contaDeOrigemDeveAceitarContaCom6DigitosNumericos() {
		builder.withContaOrigem("123456");
	}

	@Test(expected = IllegalArgumentException.class)
	public void contaDeOrigemDeveLancarExcecaoAoReceberContaComMenosDe6Caracteres() {
		builder.withContaOrigem("12345");
	}

	@Test(expected = IllegalArgumentException.class)
	public void contaDeOrigemDeveLancarExcecaoAoReceberContaComMaisDe6Caracteres() {
		builder.withContaOrigem("1234567");
	}

	@Test(expected = IllegalArgumentException.class)
	public void contaDeOrigemNaoDeveAceitarLetras() {
		builder.withContaOrigem("12345A");
	}

	@Test(expected = IllegalArgumentException.class)
	public void contaDeOrigemNaoDeveAceitarCaracteresEspeciais() {
		builder.withContaOrigem("12345-");
	}

	@Test(expected = IllegalArgumentException.class)
	public void contaDeOrigemNaoDeveAceitarContaNula() {
		builder.withContaOrigem(null);
	}

	@Test
	public void contaDeDestinoDeveAceitarContaCom6DigitosNumericos() {
		builder.withContaDestino("123456");
	}

	@Test(expected = IllegalArgumentException.class)
	public void contaDeDestinoDeveLancarExcecaoAoReceberContaComMenosDe6Caracteres() {
		builder.withContaDestino("12345");
	}

	@Test(expected = IllegalArgumentException.class)
	public void contaDeDestinoDeveLancarExcecaoAoReceberContaComMaisDe6Caracteres() {
		builder.withContaDestino("1234567");
	}

	@Test(expected = IllegalArgumentException.class)
	public void contaDeDestinoNaoDeveAceitarLetras() {
		builder.withContaDestino("12345A");
	}

	@Test(expected = IllegalArgumentException.class)
	public void contaDeDestinoNaoDeveAceitarCaracteresEspeciais() {
		builder.withContaDestino("12345-");
	}

	@Test(expected = IllegalArgumentException.class)
	public void contaDeDestinoNaoDeveAceitarContaNula() {
		builder.withContaDestino(null);
	}

	public void dataDeTransferenciaDeveAceitarDataDeHojeOuPosterior() {
		builder.withDataTransferencia(LocalDate.now());
		builder.withDataTransferencia(LocalDate.now().plus(10L, ChronoUnit.DAYS));
	}

	@Test(expected = IllegalArgumentException.class)
	public void dataDeTransferenciaNaoDeveAceitarDataAnteriorAHoje() {
		LocalDate hoje = LocalDate.now();
		LocalDate ontem = hoje.minus(1L, ChronoUnit.DAYS);
		builder.withDataTransferencia(ontem);
	}

	@Test(expected = IllegalArgumentException.class)
	public void dataDeTransferenciaNaoDeveAceitarNulo() {
		builder.withDataTransferencia(null);
	}

	@Test
	public void valorDeTransferenciaDeveAceitarValorSuperiorAZero() {
		builder.withValorTransferencia(BigDecimal.TEN);
	}

	@Test(expected = IllegalArgumentException.class)
	public void valorDeTransferenciaNaoDeveAceitarNulo() {
		builder.withValorTransferencia(null);
	}

	@Test(expected = IllegalArgumentException.class)
	public void valorDeTransferenciaNaoDeveAceitar0() {
		builder.withValorTransferencia(BigDecimal.ZERO);
	}

	@Test(expected = IllegalArgumentException.class)
	public void valorDeTransferenciaNaoDeveAceitarValoresNegativos() {
		builder.withValorTransferencia(new BigDecimal(-1));
	}

	@Test(expected = IllegalStateException.class)
	public void builderDeveLancarExcecaoAoTentarFazerBuildDeObjetoComCamposNulos() {
		builder.build();
	}

	@Test(expected = IllegalStateException.class)
	public void builderDeveLancarExcecaoAoTentarFazerBuildMaisDeUmaVez() {
		builder
			.withContaDestino("123456")
			.withContaOrigem("123456")
			.withDataTransferencia(LocalDate.now())
			.withValorTransferencia(BigDecimal.TEN)
			.withTipoOperacao(TipoOperacao.A)
			.build();

		builder.build();
	}

	@Test
	public void builderDeveRetornarObjetoOperacaoComValoresSetadosCorretamente() {
		String contaOrigem = "123456";
		String contaDestino = "987654";
		LocalDate dataTransferencia = LocalDate.now().plus(10L, ChronoUnit.DAYS);
		BigDecimal valor = BigDecimal.TEN;
		TipoOperacao tipoOperacao = TipoOperacao.B;

		Operacao operacao = builder
				.withContaOrigem(contaOrigem)
				.withContaDestino(contaDestino)
				.withDataTransferencia(dataTransferencia)
				.withValorTransferencia(valor)
				.withTipoOperacao(tipoOperacao)
				.build();

		assertThat(operacao.getContaOrigem(), is(contaOrigem));
		assertThat(operacao.getContaDestino(), is(contaDestino));
		assertThat(operacao.getDataTransferencia(), is(dataTransferencia));
		assertThat(operacao.getValorTransferencia(), is(valor));
		assertThat(operacao.getTipoOperacao(), is(operacao.getTipoOperacao()));
	}
}