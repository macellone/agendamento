package br.com.macellone.agendamento.strategy;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.number.BigDecimalCloseTo.closeTo;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

import org.junit.Before;
import org.junit.Test;

import br.com.macellone.agendamento.model.Operacao;
import br.com.macellone.agendamento.model.OperacaoBuilder;
import br.com.macellone.agendamento.model.TipoOperacao;


public class OperacaoDTest {

	private Operacao operacao;
	private OperacaoD operacaoD;

	private static final String CONTA = "123456";

	@Before
	public void before() {
		operacaoD = new OperacaoD();
	}
	
	@Test
	public void calcularDeveRetorna33ParaTransferenciaDe1000NoMesmoDia() {
		operacao = new OperacaoBuilder()
				.withContaOrigem(CONTA)
				.withContaDestino(CONTA)
				.withValorTransferencia(BigDecimal.valueOf(1000))
				.withDataTransferencia(LocalDate.now())
				.withTipoOperacao(TipoOperacao.D)
				.build();
		
		BigDecimal taxa = operacaoD.calcular(operacao);
		assertThat(taxa, closeTo(BigDecimal.valueOf(33), BigDecimal.valueOf(0.01)));
	}
	
	@Test(expected = Exception.class)
	public void calcularLancarExcecaoParaTransferenciaDe1000Amanha() {
		operacao = new OperacaoBuilder()
				.withContaOrigem(CONTA)
				.withContaDestino(CONTA)
				.withValorTransferencia(BigDecimal.valueOf(1000))
				.withDataTransferencia(LocalDate.now().plus(1L, ChronoUnit.DAYS))
				.withTipoOperacao(TipoOperacao.D)
				.build();
		
		operacaoD.calcular(operacao);
	}
	
	@Test
	public void calcularDeveRetorna12ParaTransferenciaDe2000AgendadaPara10Dias() {
		operacao = new OperacaoBuilder()
				.withContaOrigem(CONTA)
				.withContaDestino(CONTA)
				.withValorTransferencia(BigDecimal.valueOf(2000))
				.withDataTransferencia(LocalDate.now().plus(10L, ChronoUnit.DAYS))
				.withTipoOperacao(TipoOperacao.D)
				.build();
		
		BigDecimal taxa = operacaoD.calcular(operacao);
		assertThat(taxa, closeTo(BigDecimal.valueOf(12), BigDecimal.valueOf(0.01)));
	}
	
	@Test(expected = Exception.class)
	public void calcularDeveLancarExcecaoParaTransferenciaDe2000AgendadaPara11Dias() {
		operacao = new OperacaoBuilder()
				.withContaOrigem(CONTA)
				.withContaDestino(CONTA)
				.withValorTransferencia(BigDecimal.valueOf(2000))
				.withDataTransferencia(LocalDate.now().plus(11L, ChronoUnit.DAYS))
				.withTipoOperacao(TipoOperacao.D)
				.build();
		
		operacaoD.calcular(operacao);
	}
	
	@Test
	public void calcularDeveRetorna820ParaTransferenciaDe10000AgendadaPara11Dias() {
		operacao = new OperacaoBuilder()
				.withContaOrigem(CONTA)
				.withContaDestino(CONTA)
				.withValorTransferencia(BigDecimal.valueOf(10000))
				.withDataTransferencia(LocalDate.now().plus(11L, ChronoUnit.DAYS))
				.withTipoOperacao(TipoOperacao.D)
				.build();
		
		BigDecimal taxa = operacaoD.calcular(operacao);
		assertThat(taxa, closeTo(BigDecimal.valueOf(820), BigDecimal.valueOf(0.01)));
	}
	
	@Test
	public void calcularDeveRetorna690ParaTransferenciaDe10000AgendadaPara21Dias() {
		operacao = new OperacaoBuilder()
				.withContaOrigem(CONTA)
				.withContaDestino(CONTA)
				.withValorTransferencia(BigDecimal.valueOf(10000))
				.withDataTransferencia(LocalDate.now().plus(21L, ChronoUnit.DAYS))
				.withTipoOperacao(TipoOperacao.D)
				.build();
		
		BigDecimal taxa = operacaoD.calcular(operacao);
		assertThat(taxa, closeTo(BigDecimal.valueOf(690), BigDecimal.valueOf(0.01)));
	}
	
	@Test
	public void calcularDeveRetorna470ParaTransferenciaDe10000AgendadaPara31Dias() {
		operacao = new OperacaoBuilder()
				.withContaOrigem(CONTA)
				.withContaDestino(CONTA)
				.withValorTransferencia(BigDecimal.valueOf(10000))
				.withDataTransferencia(LocalDate.now().plus(31L, ChronoUnit.DAYS))
				.withTipoOperacao(TipoOperacao.D)
				.build();
		
		BigDecimal taxa = operacaoD.calcular(operacao);
		assertThat(taxa, closeTo(BigDecimal.valueOf(470), BigDecimal.valueOf(0.01)));
	}
	
	@Test
	public void calcularDeveRetorna170NegativosParaTransferenciaDe10000AgendadaPara41Dias() {
		operacao = new OperacaoBuilder()
				.withContaOrigem(CONTA)
				.withContaDestino(CONTA)
				.withValorTransferencia(BigDecimal.valueOf(10000))
				.withDataTransferencia(LocalDate.now().plus(41L, ChronoUnit.DAYS))
				.withTipoOperacao(TipoOperacao.D)
				.build();
		
		BigDecimal taxa = operacaoD.calcular(operacao);
		assertThat(taxa, closeTo(BigDecimal.valueOf(-170), BigDecimal.valueOf(0.01)));
	}
	
	@Test(expected = Exception.class)
	public void calcularDeveLancarExcecaoParaTransferenciaDe10000AgendadaParaMenosDe11Dias() {
		operacao = new OperacaoBuilder()
				.withContaOrigem(CONTA)
				.withContaDestino(CONTA)
				.withValorTransferencia(BigDecimal.valueOf(10000))
				.withDataTransferencia(LocalDate.now().plus(10L, ChronoUnit.DAYS))
				.withTipoOperacao(TipoOperacao.D)
				.build();
		
		operacaoD.calcular(operacao);
	}
}