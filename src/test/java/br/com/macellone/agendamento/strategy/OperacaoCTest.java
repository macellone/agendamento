package br.com.macellone.agendamento.strategy;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.number.BigDecimalCloseTo.closeTo;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

import org.junit.Before;
import org.junit.Test;

import br.com.macellone.agendamento.model.Operacao;
import br.com.macellone.agendamento.model.OperacaoBuilder;
import br.com.macellone.agendamento.model.TipoOperacao;

public class OperacaoCTest {
	
	private Operacao operacao;
	private OperacaoC operacaoC;
	
	private static final String CONTA = "123456";

	@Before
	public void before() {
		operacaoC = new OperacaoC();
	}
	
	@Test
	public void calcularDeveDevolver8E20ParaTransferenciaDe100AgendadaDaquiA11E20Dias() {
		operacao = new OperacaoBuilder()
				.withContaOrigem(CONTA)
				.withContaDestino(CONTA)
				.withValorTransferencia(BigDecimal.valueOf(100))
				.withDataTransferencia(LocalDate.now().plus(11L, ChronoUnit.DAYS))
				.withTipoOperacao(TipoOperacao.C)
				.build();

		BigDecimal taxa = operacaoC.calcular(operacao);
		assertThat(taxa, closeTo(BigDecimal.valueOf(8.2), BigDecimal.valueOf(0.01)));
		
		operacao = new OperacaoBuilder()
				.withContaOrigem(CONTA)
				.withContaDestino(CONTA)
				.withValorTransferencia(BigDecimal.valueOf(100))
				.withDataTransferencia(LocalDate.now().plus(20L, ChronoUnit.DAYS))
				.withTipoOperacao(TipoOperacao.C)
				.build();

		taxa = operacaoC.calcular(operacao);
		assertThat(taxa, closeTo(BigDecimal.valueOf(8.2), BigDecimal.valueOf(0.01)));
	}
	
	@Test(expected = Exception.class)
	public void calcularLancarExcecaoParaTransferenciaAgendadaAMenosDe11Dias() {
		operacao = new OperacaoBuilder()
				.withContaOrigem(CONTA)
				.withContaDestino(CONTA)
				.withValorTransferencia(BigDecimal.valueOf(100))
				.withDataTransferencia(LocalDate.now().plus(10L, ChronoUnit.DAYS))
				.withTipoOperacao(TipoOperacao.C)
				.build();

		operacaoC.calcular(operacao);
	}
	
	@Test
	public void calcularDeveDevolver6E9ParaTransferenciaDe100AgendadaDaquiA21E30Dias() {
		operacao = new OperacaoBuilder()
				.withContaOrigem(CONTA)
				.withContaDestino(CONTA)
				.withValorTransferencia(BigDecimal.valueOf(100))
				.withDataTransferencia(LocalDate.now().plus(21L, ChronoUnit.DAYS))
				.withTipoOperacao(TipoOperacao.C)
				.build();

		BigDecimal taxa = operacaoC.calcular(operacao);
		assertThat(taxa, closeTo(BigDecimal.valueOf(6.9), BigDecimal.valueOf(0.01)));
		
		operacao = new OperacaoBuilder()
				.withContaOrigem(CONTA)
				.withContaDestino(CONTA)
				.withValorTransferencia(BigDecimal.valueOf(100))
				.withDataTransferencia(LocalDate.now().plus(30L, ChronoUnit.DAYS))
				.withTipoOperacao(TipoOperacao.C)
				.build();

		taxa = operacaoC.calcular(operacao);
		assertThat(taxa, closeTo(BigDecimal.valueOf(6.9), BigDecimal.valueOf(0.01)));
	}
	
	@Test
	public void calcularDeveDevolver4E7ParaTransferenciaDe100AgendadaDaquiA31E40Dias() {
		operacao = new OperacaoBuilder()
				.withContaOrigem(CONTA)
				.withContaDestino(CONTA)
				.withValorTransferencia(BigDecimal.valueOf(100))
				.withDataTransferencia(LocalDate.now().plus(31L, ChronoUnit.DAYS))
				.withTipoOperacao(TipoOperacao.C)
				.build();

		BigDecimal taxa = operacaoC.calcular(operacao);
		assertThat(taxa, closeTo(BigDecimal.valueOf(4.7), BigDecimal.valueOf(0.01)));
		
		operacao = new OperacaoBuilder()
				.withContaOrigem(CONTA)
				.withContaDestino(CONTA)
				.withValorTransferencia(BigDecimal.valueOf(100))
				.withDataTransferencia(LocalDate.now().plus(40L, ChronoUnit.DAYS))
				.withTipoOperacao(TipoOperacao.C)
				.build();

		taxa = operacaoC.calcular(operacao);
		assertThat(taxa, closeTo(BigDecimal.valueOf(4.7), BigDecimal.valueOf(0.01)));
	}
	
	@Test
	public void calcularDeveDevolver1E7NegativoParaTransferenciaDe100AgendadaDaquiAMaisDe40Dias() {
		operacao = new OperacaoBuilder()
				.withContaOrigem(CONTA)
				.withContaDestino(CONTA)
				.withValorTransferencia(BigDecimal.valueOf(100))
				.withDataTransferencia(LocalDate.now().plus(41L, ChronoUnit.DAYS))
				.withTipoOperacao(TipoOperacao.C)
				.build();

		BigDecimal taxa = operacaoC.calcular(operacao);
		assertThat(taxa, closeTo(BigDecimal.valueOf(-1.7), BigDecimal.valueOf(0.01)));
	}
}
