package br.com.macellone.agendamento.strategy;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.number.BigDecimalCloseTo.closeTo;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

import org.junit.Before;
import org.junit.Test;

import br.com.macellone.agendamento.model.Operacao;
import br.com.macellone.agendamento.model.OperacaoBuilder;
import br.com.macellone.agendamento.model.TipoOperacao;

public class OperacaoBTest {

	private Operacao operacao;
	private OperacaoB operacaoB;
	
	private static final String CONTA = "123456";

	@Before
	public void before() {
		operacaoB = new OperacaoB();
	}
	
	@Test
	public void calcularDeveDevolver12ParaTransferenciaDe100() {
		operacao = new OperacaoBuilder()
				.withContaOrigem(CONTA)
				.withContaDestino(CONTA)
				.withValorTransferencia(BigDecimal.valueOf(100))
				.withDataTransferencia(LocalDate.now().plus(10L, ChronoUnit.DAYS))
				.withTipoOperacao(TipoOperacao.B).build();

		BigDecimal taxa = operacaoB.calcular(operacao);
		assertThat(taxa, closeTo(BigDecimal.valueOf(12), BigDecimal.valueOf(0.01)));
	}
	
	@Test(expected = Exception.class)
	public void calcularDeveLancarExcecaoAoReceberDataFuturaSuperiorA10DiasComTipoDeOperacaoB() {
		operacao = new OperacaoBuilder()
				.withContaOrigem(CONTA)
				.withContaDestino(CONTA)
				.withValorTransferencia(BigDecimal.valueOf(100))
				.withDataTransferencia(LocalDate.now().plus(11L, ChronoUnit.DAYS))
				.withTipoOperacao(TipoOperacao.B).build();
		
		operacaoB.calcular(operacao);
	}
}
