package br.com.macellone.agendamento.strategy;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.number.BigDecimalCloseTo.closeTo;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

import org.junit.Before;
import org.junit.Test;

import br.com.macellone.agendamento.model.Operacao;
import br.com.macellone.agendamento.model.OperacaoBuilder;
import br.com.macellone.agendamento.model.TipoOperacao;

public class OperacaoATest {

	private Operacao operacao;
	private OperacaoA operacaoA;

	private static final String CONTA = "123456";

	@Before
	public void before() {
		operacaoA = new OperacaoA();
	}

	@Test
	public void calcularDeveDevolver6ParaTransferenciaDe100() {
		operacao = new OperacaoBuilder()
				.withContaOrigem(CONTA)
				.withContaDestino(CONTA)
				.withValorTransferencia(BigDecimal.valueOf(100))
				.withDataTransferencia(LocalDate.now())
				.withTipoOperacao(TipoOperacao.A).build();

		BigDecimal taxa = operacaoA.calcular(operacao);
		assertThat(taxa, closeTo(BigDecimal.valueOf(6), BigDecimal.valueOf(0.01)));
	}

	@Test(expected = Exception.class)
	public void calcularDeveLancarExcecaoAoReceberTipoOperacaoAComDataFutura() {
		operacao = new OperacaoBuilder()
				.withContaOrigem(CONTA)
				.withContaDestino(CONTA)
				.withValorTransferencia(BigDecimal.valueOf(100))
				.withDataTransferencia(LocalDate.now().plus(10L, ChronoUnit.DAYS))
				.withTipoOperacao(TipoOperacao.A)
				.build();

		operacaoA.calcular(operacao);
	}
}
